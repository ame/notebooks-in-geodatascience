images,source
logo-osgeo.svg,https://github.com/OSGeo/osgeo/tree/master/marketing/branding/logo
logo_jupyter.svg,https://jupyter.org/assets/homepage/main-logo.svg
grass_notebook.png,https://github.com/ncsu-geoforall-lab/grass-gis-workshop-foss4g-2022/blob/main/workshop_part_1_intro.ipynb
logo-rmarkdown.png,https://pkgs.rstudio.com/rmarkdown/
